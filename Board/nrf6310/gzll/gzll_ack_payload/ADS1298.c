


#include "ADS1298.h"
#include "spi_master.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "spi_master_config.h"


static uint8_t command_byte;
static uint8_t dummy_byte;
static uint8_t read_register[3];
static uint8_t write_register[3];
static uint8_t testbyte;
static bool spitest;


void ads_init(uint32_t *spi_base_address)
{
	  //testbyte = 0;
    dummy_byte = 0;	
	  nrf_gpio_port_set(NRF_GPIO_PORT_SELECT_PORT3, 0x01);
	  nrf_delay_us(1000000); 
	  nrf_gpio_port_clear(NRF_GPIO_PORT_SELECT_PORT3, 0x01);
	  nrf_delay_us(100000); 
	  nrf_gpio_port_set(NRF_GPIO_PORT_SELECT_PORT3, 0x01);
	  nrf_delay_us(100000);
	  //command_byte = SDATAC;
	  //spi_master_tx_rx(spi_base_address, 1, (const uint8_t *)&command_byte, &dummy_byte);
	
		ads_opcode(spi_base_address,SDATAC);
	
		ads_write_register(spi_base_address,CONFIG3,0xC0);	
	  
	  ads_read_register(spi_base_address,CONFIG3,&testbyte);
	  //nrf_delay_us(500000);
	  ads_read_register(spi_base_address,0x00,&testbyte);
	
		ads_write_register(spi_base_address,CONFIG1,0x86);
		ads_write_register(spi_base_address,CONFIG2,0x00);
		ads_write_register(spi_base_address,CH1SET,0x01);
		ads_write_register(spi_base_address,CH2SET,0x01);
		ads_write_register(spi_base_address,CH3SET,0x01);
		ads_write_register(spi_base_address,CH4SET,0x01);
		ads_write_register(spi_base_address,CH5SET,0x01);
		ads_write_register(spi_base_address,CH6SET,0x01);
		ads_write_register(spi_base_address,CH7SET,0x01);
		ads_write_register(spi_base_address,CH8SET,0x01);
		
	  //command_byte = RDATAC;
	  //spi_master_tx_rx(spi_base_address, 1, (const uint8_t *)&command_byte, &dummy_byte);
		ads_opcode(spi_base_address,RDATAC);
	  //dummy_byte = 0;////ommmmit
		//nrf_delay_us(500000);
		//command_byte = SDATAC;
	  //spi_master_tx_rx(spi_base_address, 1, (const uint8_t *)&command_byte, &dummy_byte);
		//nrf_delay_us(500000);
		ads_opcode(spi_base_address,SDATAC);
		ads_write_register(spi_base_address,CONFIG2,0x10);
		ads_write_register(spi_base_address,CH1SET,0x05);
		ads_write_register(spi_base_address,CH2SET,0x05);
		ads_write_register(spi_base_address,CH3SET,0x05);
		ads_write_register(spi_base_address,CH4SET,0x05);
		ads_write_register(spi_base_address,CH5SET,0x05);
		ads_write_register(spi_base_address,CH6SET,0x05);
		ads_write_register(spi_base_address,CH7SET,0x05);
		ads_write_register(spi_base_address,CH8SET,0x05);
		
		ads_read_register(spi_base_address,CH6SET,&testbyte);
		//command_byte = RDATAC;
	  //spi_master_tx_rx(spi_base_address, 1, (const uint8_t *)&command_byte, &dummy_byte);
		ads_opcode(spi_base_address,RDATAC);
		
		
	
}




void ads_write_register(uint32_t *spi_base_address,uint8_t reg, uint8_t val)
{
	write_register[0] = reg|0x40;
	write_register[1] = 0;
	write_register[2] = val;
	
	spi_master_tx_rx(spi_base_address, 3, (const uint8_t *)write_register, read_register);
	
	nrf_delay_us(50000);
	
}



void ads_read_register(uint32_t *spi_base_address,uint8_t reg,uint8_t* rval)
{
	write_register[0] = reg|0x20;
	write_register[1] = 0;
	write_register[2] = 0;
	
	spitest = spi_master_tx_rx(spi_base_address, 3, (const uint8_t *)write_register, read_register);
	*rval = read_register[2];
	
	nrf_delay_us(50000);
}


void ads_opcode(uint32_t *spi_base_address,uint8_t opcode)
{
	dummy_byte = 0;
	command_byte = opcode;
	spi_master_tx_rx(spi_base_address, 1, (const uint8_t *)&command_byte, &dummy_byte);
	nrf_delay_us(50000);
}


