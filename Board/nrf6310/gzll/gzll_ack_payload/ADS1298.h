
#include <stdbool.h>
#include <stdint.h>


#define ADSSTOP        0x0A
#define ADSSTART       0x08
#define SDATAC         0x11
#define RDATAC         0x10
#define ADSRESET       0x06
#define CONFIG1        0x01
#define CONFIG2        0x02
#define CONFIG3        0x03
#define CH1SET         0x05 
#define CH2SET         0x06
#define CH3SET         0x07
#define CH4SET         0x08
#define CH5SET         0x09
#define CH6SET         0x0A
#define CH7SET         0x0B
#define CH8SET         0x0C








void ads_init(uint32_t *spi_base_address);

void ads_write_register(uint32_t *spi_base_address,uint8_t reg, uint8_t val);

void ads_read_register(uint32_t *spi_base_address,uint8_t reg,uint8_t* rval);

void ads_opcode(uint32_t *spi_base_address,uint8_t opcode);

