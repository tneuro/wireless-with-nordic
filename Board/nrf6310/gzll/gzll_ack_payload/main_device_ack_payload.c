/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
*
* The information contained herein is property of Nordic Semiconductor ASA.
* Terms and conditions of usage are described in detail in NORDIC
* SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
*
* Licensees are granted free, non-transferable use of the information. NO
* WARRANTY of ANY KIND is provided. This heading must NOT be removed from
* the file.
*
* $LastChangedRevision: 15516 $
*/

/** 
 * @file
 * @brief Gazell Link Layer Device with Payload in ACK example
 * @defgroup gzll_device_ack_payload_example Gazell Link Layer Device with Payload in ACK
 * @{
 * @ingroup gzll_03_examples
 *
 * This project requires that a Host running the 
 * @ref gzll_host_ack_payload_example example be used as a counterpart for 
 * receiving the data. This can be on either nRF51 device or a nRF24Lxx device
 * running the \b gzll_host_ack_payload example in the nRFgo SDK. 
 * 
 * This example sends a packet and adds a new packet to the TX queue every time
 * it receives an ACK. Before adding a packet to the TX queue, the contents of 
 * the GPIO Port BUTTONS is copied to the first payload byte (byte 0). 
 * When an ACK is received, the contents of the first payload byte of 
 * the ACK are output on GPIO Port LEDS. 
 */


#include "nrf_gzll.h"
#include "nrf_gpio.h"
#include "spi_master.h"
#include "nrf_delay.h"
#include "common.h"
#include "boards.h"
#include "spi_master_config.h"
#include "ADS1298.h"
//#include "nrf_gpiote.h"
#include "app_gpiote.h"
#include "app_error.h"
#include "app_fifo.h"
/*****************************************************************************/
/** @name Configuration */
/*****************************************************************************/

// Define pipe
#define PIPE_NUMBER 0 ///< We use pipe 0 in this example

// GPIO
#define BUTTONS NRF_GPIO_PORT_SELECT_PORT2 ///< GPIO port for reading from buttons//Ali from PORT0
#define LEDS    NRF_GPIO_PORT_SELECT_PORT2 ///< GPIO port for writing to LEDs

// Define payload length
#define TX_PAYLOAD_LENGTH 25 ///< We use 1 byte payload length when transmitting

//GPIOTE
#define MAX_USERS 1

/////////////////
#define buffer_size 512
/////////////////

// Data and acknowledgement payloads
static uint8_t data_payload[TX_PAYLOAD_LENGTH];                ///< Payload to send to Host. 
static uint8_t ack_payload[NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH]; ///< Placeholder for received ACK payloads from Host.

// Debug helper variables
static volatile bool init_ok, enable_ok, push_ok, pop_ok, tx_success,tx_done;  

////////////////////////////
static uint8_t tx_data[TX_RX_MSG_LENGTH]; /**< SPI TX buffer. */
static uint8_t rx_data[TX_RX_MSG_LENGTH]; /**< SPI RX buffer. */
static uint8_t testspi[5];
/** @} */

// GPIOTE user identifier for the example module.
static app_gpiote_user_id_t   spi_drdyb;

// GPIOTE event handler.
static void ads_gpiote_event_handler(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low);

uint32_t  low_to_high_bitmask = 0x00000000; // Bitmask to be notified of transition from low to high for GPIO 0-3
uint32_t  high_to_low_bitmask = 0x00000020; // Bitmask to be notified of transition from high to low for GPIO 0-2
uint32_t retval;
uint32_t *spi_base_address;

// Create a FIFO structure
app_fifo_t ads_fifo;

uint8_t ads_buffer[buffer_size];

static uint32_t err_code;
static uint8_t  t_counter;
static uint8_t led_toggle;
static uint32_t local_pipe;
/*****************************************************************************/
/** 
* @brief Main function. 
* 
* @return ANSI required int return type.
*/
/*****************************************************************************/
//lint -save -e514 Unusual use of a boolean expression (use of &= assignment).

int main()
{
    uint8_t debug_led_output;
	
		// Macro to initialize GPIOTE module and reserving necessary memory for each of user.
    APP_GPIOTE_INIT(MAX_USERS);
	
	  retval = app_gpiote_user_register(&spi_drdyb,
                                  low_to_high_bitmask,
                                  high_to_low_bitmask,
                                  ads_gpiote_event_handler);

		if (retval != NRF_SUCCESS)
    {
    // Failed to register with user with GPIO module! just for test ali
	 // while(1);
    }
	
    // Setup port directions
    //nrf_gpio_port_dir_set(BUTTONS, NRF_GPIO_PORT_DIR_INPUT);
	  nrf_gpio_range_cfg_input(16, 17, NRF_GPIO_PIN_PULLUP);
		nrf_gpio_cfg_input(5, NRF_GPIO_PIN_NOPULL);
	  nrf_gpio_word_byte_write(&NRF_GPIO->DIRSET, LEDS, 0x0C);//0x30
		nrf_gpio_word_byte_write(&NRF_GPIO->DIRSET, NRF_GPIO_PORT_SELECT_PORT1, 0x10);
	  //nrf_gpio_word_byte_write(&NRF_GPIO->DIRSET, NRF_GPIO_PORT_SELECT_PORT1,0x08 );
	   nrf_gpio_word_byte_write(&NRF_GPIO->DIRSET, NRF_GPIO_PORT_SELECT_PORT3,0x01 );
    //nrf_gpio_port_dir_set(LEDS, NRF_GPIO_PORT_DIR_OUTPUT);
	
		for(int i = 0;i<TX_RX_MSG_LENGTH;i++)
		{
		tx_data[i] = 0;
		}


    // Initialize Gazell
    init_ok = nrf_gzll_init(NRF_GZLL_MODE_DEVICE);
    
    // Attempt sending every packet up to 100 times    
    init_ok &= nrf_gzll_set_max_tx_attempts(100);

    // Load data into TX queue
    data_payload[0] = nrf_gpio_port_read(BUTTONS);  
    push_ok = nrf_gzll_add_packet_to_tx_fifo(PIPE_NUMBER, data_payload, TX_PAYLOAD_LENGTH);
    
    // Enable Gazell to start sending over the air
    enable_ok = nrf_gzll_enable();         
	
	////////////////////////
	  spi_base_address = spi_master_init(SPI0, SPI_MODE1, false);
		
		err_code = app_fifo_init(&ads_fifo, ads_buffer, (uint16_t)sizeof(ads_buffer));
		
		ads_init(spi_base_address);
    
    retval = app_gpiote_user_enable(spi_drdyb);
		if (retval != NRF_SUCCESS)
    {
    // Failed to register with user with GPIO module! just for test ali
			nrf_delay_us(1000000); // 1 second delay
	    while(1);
    }
		
		spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
		
    while(1)
    {
        // Error handling
        debug_led_output = ((uint8_t)tx_success << 4) | ((uint8_t)pop_ok << 3) | ((uint8_t)push_ok << 2) | ((uint8_t)enable_ok << 1) | (uint8_t)init_ok;                
        
			////////////////
			  //spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
			//ads_read_register(spi_base_address,0x00,testspi);
			/////////////////////
			if(tx_success&tx_done)
			{
				
									// Read buttons and load data payload into TX queue
								//data_payload[0] = nrf_gpio_port_read(BUTTONS);
	
				for(int i = 0;i<25;i++)
					{
						err_code = app_fifo_get(&ads_fifo, data_payload+i);
					}
				if(err_code != NRF_ERROR_NOT_FOUND)
				{
					push_ok = nrf_gzll_add_packet_to_tx_fifo(local_pipe, data_payload, TX_PAYLOAD_LENGTH);
					tx_done = false;
					if(led_toggle == 0)
					{
					//nrf_gpio_port_write(NRF_GPIO_PORT_SELECT_PORT1,0x10);
					nrf_gpio_word_byte_write(&NRF_GPIO->OUTSET, NRF_GPIO_PORT_SELECT_PORT1, 0x10);
					led_toggle = 1;
					}
					else
					{
						//nrf_gpio_port_write(NRF_GPIO_PORT_SELECT_PORT1,0x00);
						nrf_gpio_word_byte_write(&NRF_GPIO->OUTCLR, NRF_GPIO_PORT_SELECT_PORT1, 0x10);
						led_toggle = 0;
					}
				}
				
				
			}
			
        // If an error has occured
        //if(debug_led_output != 0x1F)
			  if(debug_led_output != 0x17)//with no pop
        {
            nrf_gpio_port_write(LEDS,0x0F); //ali 0xFF     
            nrf_delay_us(1000000); // 1 second delay 
            nrf_gpio_port_write(LEDS, debug_led_output&0x0F);//Ali 
            nrf_delay_us(1000000); // 1 second delay 
            nrf_gpio_port_write(LEDS,0x00 + (uint32_t)nrf_gzll_get_error_code()); //Ali 0xF0
            nrf_delay_us(1000000); // 1 second delay 
					//while(1);
        }

        // Optionally send the CPU to sleep while waiting for a callback.
        // __WFI();
    }
}


/*****************************************************************************/
/** @name Gazell callback function definitions  */
/*****************************************************************************/


// If an ACK was received, we send another packet. 
void  nrf_gzll_device_tx_success(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info)
{
    uint32_t ack_payload_length = NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH;    
    tx_success = true;
		tx_done = true;
    local_pipe = pipe;
   // if (tx_info.payload_received_in_ack)
    //{
        // Pop packet and write first byte of the payload to the GPIO port.
      //  pop_ok = nrf_gzll_fetch_packet_from_rx_fifo(pipe, ack_payload, &ack_payload_length);
       // if (ack_payload_length > 0)
        //{
         //   nrf_gpio_port_write(LEDS, ~ack_payload[0]);  // Button press is active low. 
        //}
    //}
	
		
			

}


/* If the transmission failed, send a new packet.
 * This callback does not occur by default since NRF_GZLL_DEFAULT_MAX_TX_ATTEMPTS 
 * is 0 (inifinite retransmits)
 */
void nrf_gzll_device_tx_failed(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info)
{
    tx_success = false;

    // Load data into TX queue
    //data_payload[0] = nrf_gpio_port_read(BUTTONS);
    //push_ok = nrf_gzll_add_packet_to_tx_fifo(pipe, data_payload, TX_PAYLOAD_LENGTH);
		push_ok = nrf_gzll_add_packet_to_tx_fifo(pipe, data_payload, TX_PAYLOAD_LENGTH);
	  
}



// Callbacks not needed in this example.
void nrf_gzll_host_rx_data_ready(uint32_t pipe, nrf_gzll_host_rx_info_t rx_info)
{}
void nrf_gzll_disabled()
{}

/** @} */
/** @} */

	void ads_gpiote_event_handler(uint32_t event_pins_low_to_high, uint32_t event_pins_high_to_low)
	{
		
		if (event_pins_high_to_low & 0x00000020)
    {
         // GPIO pin 2 transitioned from high to low.
         // Take necessary action.
			spi_master_tx_rx(spi_base_address, TX_RX_MSG_LENGTH, (const uint8_t *)tx_data, rx_data);
			for ( int i = 0;i<24;i++)
			{
				err_code = app_fifo_put(&ads_fifo, *(rx_data+3+i));
			}
			err_code = app_fifo_put(&ads_fifo,t_counter++);
			
			
    }
	}
	


