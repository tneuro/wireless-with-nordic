/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
*
* The information contained herein is property of Nordic Semiconductor ASA.
* Terms and conditions of usage are described in detail in NORDIC
* SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
*
* Licensees are granted free, non-transferable use of the information. NO
* WARRANTY of ANY KIND is provided. This heading must NOT be removed from
* the file.
*
* $LastChangedRevision: 15516 $
*/

/** 
 * @file
 * @brief Gazell Link Layer Host with Payload in ACK example
 * @defgroup gzll_host_ack_payload_example Gazell Link Layer Host with Payload in ACK
 * @{
 * @ingroup gzll_03_examples
 *
 * This project requires that a Device running the 
 * @ref gzll_device_ack_payload_example be used as a counterpart for 
 * receiving the data. This can be on either nRF51 device or a nRF24Lxx device
 * running the \b gzll_device_ack_payload example in the nRFgo SDK. 
 * 
 * This example listens for a packet and sends an ACK
 * when a packet is received. The contents of the first payload byte of 
 * the received packet is output on the GPIO Port BUTTONS. 
 * The contents of GPIO Port LEDS are sent in the first payload byte (byte 0) 
 * of the ACK packet.
 */


#include "nrf_gzll.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include <stdbool.h>
#include <stdint.h>
#include "nrf.h"
#include "simple_uart.h"
#include "boards.h"

/*****************************************************************************/
/** @name Configuration  */
/*****************************************************************************/

// Define pipe
#define PIPE_NUMBER 0 ///< We use pipe 0 in this example

// GPIO
#define BUTTONS NRF_GPIO_PORT_SELECT_PORT2 ///< GPIO port for reading from buttons
#define LEDS    NRF_GPIO_PORT_SELECT_PORT2 ///< GPIO port for writing to LEDs

// Define payload length
#define TX_PAYLOAD_LENGTH 1 ///< We use 1 byte payload length when transmitting

// Data and acknowledgement payloads
static uint8_t data_payload[NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH];  ///< Placeholder for data payload received from host. 
static uint8_t ack_payload[TX_PAYLOAD_LENGTH];                   ///< Payload to attach to ACK sent to device.

// Debug helper variables
extern nrf_gzll_error_code_t nrf_gzll_error_code;   ///< Error code
static bool init_ok, enable_ok, push_ok, pop_ok, packet_received;  

///////////////////////

#define ERROR_PIN                (8)   /*!< gpio pin number to show error if loopback is enabled */
#define MAX_TEST_DATA_BYTES      (15U) /*!< max number of test bytes to be used for tx and rx */

#ifndef ENABLE_LOOPBACK_TEST

/** @brief Function for sending ' Exit!' string to UART.
Execution is blocked until UART peripheral detects all characters have been sent.
 */
static __INLINE void uart_quit()
{
  simple_uart_putstring((const uint8_t *)" \n\rAliExit!\n\r");
}

/** @brief Function for sending 'Start: ' string to UART.
Execution is blocked until UART peripheral detects all characters have been sent.
 */
static __INLINE void uart_start()
{
  simple_uart_putstring((const uint8_t *)" \n\rStart: ");
}

#else

/** @brief Function for setting the @ref ERROR_PIN to one and enter an infinite loop. This function is called if any of the
 *  nRF6350 functions fail.
 */
static void show_error(void)
{
  nrf_gpio_pin_write(ERROR_PIN, 1);
  while(true)
  {
  }
}


/** @brief Function for transmitting one char at a time and check if the loopback received data is the same as transmitted.
 *  Just used for testing with loopback setup (i.e, @ref TX_PIN_NUMBER connected to @ref RX_PIN_NUMBER)
 *  return true if test passed, else return false
 */
static void uart_loopback_test()
{
  uint8_t *tx_data = (uint8_t *)("\n\rLOOPBACK_TEST");
  uint8_t rx_data;

  // Start sending one byte and see if you get the same
  for(uint8_t i = 0; i < MAX_TEST_DATA_BYTES; i++)
  {
    bool status;
    simple_uart_put(tx_data[i]);
    status = simple_uart_get_with_timeout(2, &rx_data);

    if ((rx_data != tx_data[i]) || (!status))
    {
      show_error();
    }
  }
  return; // Test passed
}

#endif

/** @} */

/*****************************************************************************/
/**
* @brief Main function.
* @return ANSI required int return type.
*/
/*****************************************************************************/
int main()
{
    uint8_t debug_led_output;

    // Setup port directions
    nrf_gpio_port_dir_set(BUTTONS,NRF_GPIO_PORT_DIR_INPUT);
    nrf_gpio_port_dir_set(LEDS,NRF_GPIO_PORT_DIR_OUTPUT);
    
    // Initialize Gazell
    init_ok = nrf_gzll_init(NRF_GZLL_MODE_HOST);  
    
    // Load data into TX queue
    ack_payload[0] = nrf_gpio_port_read(BUTTONS);  // Button logic is inverted.
    push_ok = nrf_gzll_add_packet_to_tx_fifo(PIPE_NUMBER, data_payload, TX_PAYLOAD_LENGTH);

    // Enable Gazell to start sending over the air
    enable_ok = nrf_gzll_enable();
	
	/////////////
	
	simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC);



  uart_start();

    while(1)
    {
        // Error handling
        debug_led_output = ((uint8_t)packet_received << 4) + ((uint8_t)pop_ok << 3) + ((uint8_t)push_ok << 2) + ((uint8_t)enable_ok << 1) + (uint8_t)init_ok;                
        
        // If an error has occured
        if(debug_led_output != 0x1F)
        {
            nrf_gpio_port_write(LEDS,0xFF);      
            nrf_delay_us(1000000); // 1 second delay 
            nrf_gpio_port_write(LEDS, debug_led_output);
            nrf_delay_us(1000000); // 1 second delay 
            nrf_gpio_port_write(LEDS,0xF0 + (uint32_t)nrf_gzll_get_error_code()); 
            nrf_delay_us(1000000); // 1 second delay 
        }
    uint8_t cr = simple_uart_get();
    simple_uart_put(cr);

    if(cr == 'q' || cr == 'Q')
    {
      uart_quit();
      while(1){}
    }
        // Optionally send the CPU to sleep while waiting for a callback.
        // __WFI();
    }
}


/*****************************************************************************/
/** @name Gazell callback function definitions  */
/*****************************************************************************/


// If a data packet was received, we write the first byte to LEDS. 
void nrf_gzll_host_rx_data_ready(uint32_t pipe, nrf_gzll_host_rx_info_t rx_info)
{   
    uint32_t data_payload_length = NRF_GZLL_CONST_MAX_PAYLOAD_LENGTH;
    packet_received = true;

    // Pop packet and write first byte of the payload to the GPIO port.
    pop_ok = nrf_gzll_fetch_packet_from_rx_fifo(pipe, data_payload, &data_payload_length);
    if (data_payload_length > 0)
    {
        nrf_gpio_port_write(LEDS,(~(data_payload[0]<<5))&0x60);
			  uint32_t testtt = data_payload[0]&0x03;
			  if (testtt == 0x00000001)
				{
					simple_uart_putstring((const uint8_t *)" \n\rNobuttons\n\r");
					
				}
    }

    // Read buttons and load ACK payload into TX queue
    ack_payload[0] = nrf_gpio_port_read(BUTTONS);  // Button logic is inverted.
    push_ok = nrf_gzll_add_packet_to_tx_fifo(pipe, ack_payload, TX_PAYLOAD_LENGTH);

}

// Callbacks not needed in this example.
void nrf_gzll_device_tx_success(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info) {}
void nrf_gzll_device_tx_failed(uint32_t pipe, nrf_gzll_device_tx_info_t tx_info) {}
void nrf_gzll_disabled() {}

/** @} */
/** @} */

